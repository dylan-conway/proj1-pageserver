# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

The software sets up a server that can serve pages. It is able to recognize forbidden pages and pages that are not found.

## Author: Dylan Conway, dconway@uoregon.edu ##

